const amqp = require('amqplib/callback_api');
const crypto = require('crypto')
const env = process.env.NODE_ENV || 'localhost'
const appconfig = require('./configs/app.config.json')
const rbmqconfig = appconfig.rbmqsetting[env]
const jwt = require('jsonwebtoken');

let utils = {
    writeToMeeberianHistoryTable: (schema, data) => {
        schema.create(data).then((response) => {
            return (response == null)
        })
    },
    createAdminToken: () => {
        var user = {
            isAdmin: true,
        }
        let options = { expiresIn: appconfig.ttltoken };
        var token = jwt.sign(user, appconfig.secretkey);
        return token
    },
    createUserTokenWithUserID: (userid) => {
        var user = {
            userid: userid
        }
        let options = { expiresIn: appconfig.ttltoken };
        var token = jwt.sign(user, appconfig.secretkey, options);
        return token
    },
    checkToken: function(token, cb) {
        if (token == undefined){
            cb(false)
        }
        jwt.verify(token, appconfig.secretkey, (err, decoded) => {
            if (err) {
                cb(false)
            } else {
                cb(decoded)
            }
        })
    },
    connectAMQP: (exname) => {
        amqp.connect(rbmqconfig.rbmqurl + '?heartbeat=60', (err, conn) => {
            if (err || conn == undefined) {
                console.error('[AMQP: '+exname+'] conn error', err.message)
                utils.connectAMQP(rbmqconfig.rbmqexchange)
                return
            }
            conn.on('error', (err) => {
                if (err.messsage !== 'Connection closing') {
                    console.error('[AMQP:'+exname+'] conn error', err.message)
                    utils.connectAMQP(rbmqconfig.rbmqexchange)
                }
            })
            conn.on('close', () => {
                console.error('[AMQP:'+exname+'] reconnecting')
            })
            console.log('[AMQP:'+exname+'] connected to exchange',exname, rbmqconfig.rbmqurl);
            conn.createChannel((err, ch) => {
                if (err) {
                    console.error('[AMQP:'+exname+'] error', err);
                    amqp.close();
                } else {
                    ch.on('error', (err) => {
                        console.error('[AMQP:'+exname+'] channel error', err.message)
                    })
                    ch.on('close', (err) => {
                        console.error('[AMQP:'+exname+'] channel closed')
                    })
                    console.error('[AMQP:'+exname+'] channel opened')
                    ch.assertExchange(exname, 'fanout', {durable: false})
                    amqpConn = ch
                    ch.assertQueue('', {durable: true}, (err, q) => {
                        if (err) {
                            console.error('[AMQP:'+exname+'] error', err);
                            conn.close();
                        } else {
                            ch.bindQueue(q.queue, exname, '')
                            ch.consume(q.queue, processMsg, {noAck: false});
                            console.log('[AMQP:'+exname+'] Consumer is connected to', exname);
                        }
                    })
                    function processMsg(msg) {
                        ch.ack(msg)
                    }
                }
            })
        })
    },
    doPublishToAMQPExchange: (exchange, param) => {
        let content = new Buffer(JSON.stringify(param))
        amqpConn.publish(exchange, 'jobs', content)
        console.log('[AMQP:'+exchange+'] published to', exchange, param)
    },
    encryptAES(text){
        var cipher = crypto.createCipher('aes-256-cbc',appconfig.secretkey)
        var crypted = cipher.update(text,'utf8','hex')
        crypted += cipher.final('hex')
        return crypted
    },
    decryptAES(text){
        var decipher = crypto.createDecipher('aes-256-cbc',appconfig.secretkey)
        var dec = decipher.update(text,'hex','utf8')
        dec += decipher.final('utf8')
        return dec
    }
}

module.exports = utils
