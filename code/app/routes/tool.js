const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const bcrypt = require('bcrypt')
const request = require('request')
const rendom = require('rendom')
const utils = require('../utils.js')
const path = require('path')

router.get('/otp', (req, res) => {
    if (!req.query.phone) {
        res.send('Need phone number')
    } else {
        req.otp.findOne({
            where: {
                phone: req.query.phone
            }
        }).then((result) => {
            if (!result) {
                let retval = {
                    code: 404,
                    message: 'OTP Not Found',
                    data: null
                }
                res.status(404).json(retval)
            } else {
                let retval = {
                    code: 200,
                    message: 'OTP Found',
                    data: result
                }
                res.status(200).json(retval)
            }
        }).catch((error) => {
            res.status(500).end(error.message)
        })
    }
})


module.exports = router
